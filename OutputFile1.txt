

passenger table:
+----------------+----------+--------+---------------+------------------+----------+----------+
| Passenger_name | Category | Gender | Boarding_City | Destination_City | Distance | Bus_Type |
+----------------+----------+--------+---------------+------------------+----------+----------+
| Sejal          | AC       | F      | Bengaluru     | Chennai          |      350 | Sleeper  |
| Anmol          | Non-AC   | M      | Mumbai        | Hyderabad        |      700 | Sitting  |
| Pallavi        | AC       | F      | panaji        | Bengaluru        |      600 | Sleeper  |
| Khusboo        | AC       | F      | Chennai       | Mumbai           |     1500 | Sleeper  |
| Udit           | Non-AC   | M      | Trivandrum    | panaji           |     1000 | Sleeper  |
| Ankur          | AC       | M      | Nagpur        | Hyderabad        |      500 | Sitting  |
| Hemant         | Non-AC   | M      | panaji        | Mumbai           |      700 | Sleeper  |
| Manish         | Non-AC   | M      | Hyderabad     | Bengaluru        |      500 | Sitting  |
| Piyush         | AC       | M      | Pune          | Nagpur           |      700 | Sitting  |
+----------------+----------+--------+---------------+------------------+----------+----------+

1. How many female and how many male passengers travelled for a minimum distance of 600 KM s? 

+----------+------------+
| male_cnt | female_cnt |
+----------+------------+
|        2 |          1 |
+----------+------------+

2.Find the minimum ticket price for Sleeper Bus?

+----------------------+
| minimum_ticket_price |
+----------------------+
|                  434 |
+----------------------+

3. Select passenger names whose names start with character 'S'?

+----------------+
| Passenger_name |
+----------------+
| Sejal          |
+----------------+

4.Calculate price charged for each passenger displaying Passenger name, Boarding City, Destination City, Bus_Type, Price in the output?

+----------------+---------------+------------------+----------+-------+
| Passenger_name | Boarding_City | Destination_City | Bus_Type | Price |
+----------------+---------------+------------------+----------+-------+
| Sejal          | Bengaluru     | Chennai          | Sleeper  |   770 |
| Pallavi        | panaji        | Bengaluru        | Sleeper  |  1320 |
| Hemant         | panaji        | Mumbai           | Sleeper  |  1540 |
| Udit           | Trivandrum    | panaji           | Sleeper  |  2200 |
| Sejal          | Bengaluru     | Chennai          | Sleeper  |   434 |
| Manish         | Hyderabad     | Bengaluru        | Sitting  |   620 |
| Ankur          | Nagpur        | Hyderabad        | Sitting  |   620 |
| Piyush         | Pune          | Nagpur           | Sitting  |   868 |
| Anmol          | Mumbai        | Hyderabad        | Sitting  |   868 |
+----------------+---------------+------------------+----------+-------+

5. What is the passenger name and his/her ticket price who travelled in Sitting bus for a distance of 1000 KMs?

Empty set (0.00 sec)

6. What will be the Sitting and Sleeper bus charge for Pallavi to travel from Bangalore to Panaji? 
+----------------+------------------+---------------+----------+-------+
| Passenger_name | Destination_city | Boarding_city | Bus_Type | Price |
+----------------+------------------+---------------+----------+-------+
| Pallavi        | panaji           | Bengaluru     | Sleeper  |  1320 |
| Pallavi        | panaji           | Bengaluru     | Sitting  |   744 |
+----------------+------------------+---------------+----------+-------+

7. List the distances from the "Passenger" table which are unique (non-repeated distances) in descending order. 

+----------+
| Distance |
+----------+
|     1500 |
|     1000 |
|      700 |
|      600 |
|      500 |
|      350 |
+----------+

8. Display the passenger name and percentage of distance travelled by that passenger from the total distance travelled by all passengers without using user variables?

+----------------+----------+------------------+
| passenger_Name | Distance | percent_of_total |
+----------------+----------+------------------+
| Sejal          |      350 |           5.3435 |
| Anmol          |      700 |          10.6870 |
| Pallavi        |      600 |           9.1603 |
| Khusboo        |     1500 |          22.9008 |
| Udit           |     1000 |          15.2672 |
| Ankur          |      500 |           7.6336 |
| Hemant         |      700 |          10.6870 |
| Manish         |      500 |           7.6336 |
| Piyush         |      700 |          10.6870 |
+----------------+----------+------------------+  
9. Create a view to see all passengers who travelled in AC Bus?
+----------------+----------+--------+---------------+------------------+----------+----------+
| Passenger_name | Category | Gender | Boarding_City | Destination_City | Distance | Bus_Type |
+----------------+----------+--------+---------------+------------------+----------+----------+
| Sejal          | AC       | F      | Bengaluru     | Chennai          |      350 | Sleeper  |
| Pallavi        | AC       | F      | panaji        | Bengaluru        |      600 | Sleeper  |
| Khusboo        | AC       | F      | Chennai       | Mumbai           |     1500 | Sleeper  |
| Ankur          | AC       | M      | Nagpur        | Hyderabad        |      500 | Sitting  |
| Piyush         | AC       | M      | Pune          | Nagpur           |      700 | Sitting  |
+----------------+----------+--------+---------------+------------------+----------+----------+

10. Create a stored procedure to find total passengers travelled using Sleeper buses?
+------------------+----------+
| total_passengers | Bus_Type |
+------------------+----------+
|                5 | Sleeper  |
+------------------+----------+

11. Display 5 records at one time 

+----------------+----------+--------+---------------+------------------+----------+----------+
| Passenger_name | Category | Gender | Boarding_City | Destination_City | Distance | Bus_Type |
+----------------+----------+--------+---------------+------------------+----------+----------+
| Sejal          | AC       | F      | Bengaluru     | Chennai          |      350 | Sleeper  |
| Anmol          | Non-AC   | M      | Mumbai        | Hyderabad        |      700 | Sitting  |
| Pallavi        | AC       | F      | panaji        | Bengaluru        |      600 | Sleeper  |
| Khusboo        | AC       | F      | Chennai       | Mumbai           |     1500 | Sleeper  |
| Udit           | Non-AC   | M      | Trivandrum    | panaji           |     1000 | Sleeper  |
+----------------+----------+--------+---------------+------------------+----------+----------+
 




